# GymFi-Rewarding-Fitness-with-Blockchain
GymFi is an innovative platform that transforms fitness achievements into a digital currency called GymFi coins. By integrating with health tracking apps, GymFi leverages blockchain technology to ensure data transparency, security, and corruption resistance.

**Team Details: **
- @dev-1: @duyhandsome.  - Role (Full Stack, Researcher, Pitch Deck) 
- Wallet address: ||0x29dBAE20Ad489E59E74bEcFCee24C8a276b61852||

- @dev-2: @taidohomework (Frontend, Backend, Pitch Deck, Forming up)
- Wallet address: ||42ks5FHLuhDLsfpuTaTxja7VMmET3uB4wdykugLkPH8N||

- @dev-3: @inuilud (Pitch Deck, Presenter)
- Wallet address: ||18HiBuHHHRWcEYb1dZRnYPYgtKag7FhuBU||

**Project Description:** 
GymFi is an innovative platform that transforms fitness achievements into a digital currency called GymFi coins. By integrating with health tracking apps, GymFi leverages blockchain technology to ensure data transparency, security, and corruption resistance. Users can earn GymFi coins based on their fat burn percentage, which can then be exchanged for NFTs or SOL coins, promoting a healthy lifestyle and providing tangible rewards.

**Pitch Deck: **
https://www.canva.com/design/DAGHW0m0PWk/RibjeoYg7wpJIByhzi1tmA/edit

https://www.canva.com/design/DAGJh8dY0lA/vMnHVJh0AV8zuBdW-lC7yA/edit?utm_content=DAGJh8dY0lA&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton

**Live Demo: **
N/A

**GitHub Link:** 
https://github.com/dominhduy09/GymFi-Rewarding-Fitness-with-Blockchain

**Is your GitHub open sourced: **
Yes


Please provide your project's Twitter Thread (A tweet announcing you're joining the "Solana Consumer Hack" Hackathon, mentioning @SuperteamVN on it Filling ALL of this in is optional, but we suggest as-many-as-possible!
